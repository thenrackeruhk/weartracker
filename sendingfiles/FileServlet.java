package cz.weissar.uhkhelperserver.files;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Paths;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

@WebServlet("/file")
@MultipartConfig
public class FileServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        /*MultipartConfigElement multipartConfigElement = new MultipartConfigElement((String) null);
        req.setAttribute(Request.__MULTIPART_CONFIG_ELEMENT, multipartConfigElement);*/

        try {
            String description = req.getParameter("description");
            Part filePart = req.getPart("file");
            String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();

            BufferedReader br = new BufferedReader(new InputStreamReader(filePart.getInputStream(), "UTF-8"));

            StringBuilder content = new StringBuilder();

            for (String line = br.readLine(); line != null; line = br.readLine()) {
                content.append(line);
            }

            br.close();
            log(content.toString());

        } catch (Exception e) {
            resp.getWriter().print(e.getMessage());
            log(e.getMessage());
        }
    }

    public void log(String content) {
        try {
            File file = new File("/props/testtest.txt");
            BufferedWriter writer = new BufferedWriter(new FileWriter(file, true));

            writer.write(content);
            writer.newLine();
            /*writer.write(category);
            writer.newLine();
            writer.write(title);
            writer.newLine();
            writer.write(body);
            writer.newLine();
            if (url != null) {
                writer.write(url);
            } else {
                writer.write("no url");
            }
            writer.newLine();
            writer.write(foundUser.getFullName());
            writer.newLine();
            writer.write(foundUser.getMail());
            writer.newLine();
            writer.write("--------");
            writer.newLine();*/

            writer.flush();
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
